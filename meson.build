project('libinputsynth', 'c', version: '0.15.0',
  meson_version: '>= 0.45.1',
)

gnome = import('gnome')

config_h = configuration_data()
config_h.set_quoted('PACKAGE_VERSION', meson.project_version())
configure_file(
  output: 'libinputsynth.h',
  configuration: config_h,
)

project_args = ['-I' + meson.build_root(),
   #'-Werror',
   '-Wall',
   '-Wextra',
]

compiler = meson.get_compiler('c')
compiler_id = compiler.get_id()

if compiler_id == 'clang'
  project_args += [
    '-Weverything',
    '-Wno-reserved-id-macro',
    '-Wno-documentation',
    '-Wno-documentation-unknown-command',
    '-Wno-padded',
    '-Wno-overlength-strings',
    '-Wno-disabled-macro-expansion',
    '-Wno-atomic-implicit-seq-cst',
    '-Wno-float-equal',
    '-Wno-used-but-marked-unused',
    '-Wno-assign-enum',
    '-Wno-gnu-folding-constant',
  ]
endif

add_project_arguments([project_args], language: ['c'])

# Paths
inputsynth_prefix = get_option('prefix')
inputsynth_libdir = join_paths(inputsynth_prefix, get_option('libdir'))
inputsynth_includedir = join_paths(inputsynth_prefix, get_option('includedir'))
inputsynth_datadir = join_paths(inputsynth_prefix, get_option('datadir'))
src_inc = include_directories('src')

gio_dep = dependency('gio-2.0', version: '>= 2.50')
glib_dep = dependency('glib-2.0')
gmodule_dep = dependency('gmodule-2.0')

x11_dep = dependency('x11', required : false)
xtst_dep = dependency('xtst', required : false)
xi_dep = dependency('xi', required : false)

cc = meson.get_compiler('c')
xdo_lib = cc.find_library('xdo', dirs : ['/usr/lib'], required : true)

libmutter_dep = dependency('libmutter-3', required: false)
mutter_clutter_dep = dependency('mutter-clutter-3', required : false)

if not libmutter_dep.found() or not mutter_clutter_dep.found()
  libmutter_dep = dependency('libmutter-4', required: false)
  mutter_clutter_dep = dependency('mutter-clutter-4', required : false)
endif

if not libmutter_dep.found() or not mutter_clutter_dep.found()
  libmutter_dep = dependency('libmutter-5', required: false)
  mutter_clutter_dep = dependency('mutter-clutter-5', required : false)
endif

subdir('src')
subdir('examples')
subdir('tests')

if get_option('api_doc')
    subdir('doc')
endif
